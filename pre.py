#author:gr
# -*- coding:utf-8 -*-

import os
import shutil

path = '/home/lixq/darknet/two_scripts/tmp'   #数据集文件夹路径，下面包含每个类，改成你自己的
new_img_path = '/Users/apple/VOCdevkit/VOC2020/JPEGImages/'   #新的图片路径，改成你自己的
new_ann_path = '/Users/apple/VOCdevkit/VOC2020/Annotations/'    #新的xml路径，搞成你自己的
count_img = 1   #每提取一张图片，count+1，也能够按顺序给图片重命名
count_ann = 1   #每提取一个xml，count+1也能够按顺序给文件重命名
path_list = os.listdir(path) #获取文件名+后缀
path_list.sort()#我对数据集没有顺序要求，只需jpg和xml一一对应，所以最简单的排序
#print(path_list)
root = path
for files in path_list:
    '''循环path文件下每个每个文件夹，每个图片，按照以.jpg结尾和.xml结尾区分'''
    #print(files)
    if files[-3:] == 'jpg':
        file_path = root + '/' + files
        shutil.copy(file_path, os.path.join(new_img_path, str(count_img).zfill(6)+'.jpg'))
        count_img = count_img + 1
    elif files[-3:] == 'xml':
        file_path = root + '/' + files
        shutil.copy(file_path, os.path.join(new_ann_path, str(count_ann).zfill(6)+'.xml'))
        count_ann = count_ann + 1
print(count_img)
print(count_ann)

